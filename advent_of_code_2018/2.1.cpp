#include <iostream>
#include <fstream>
#include <iterator>
#include <map>
int main()
{
std::ifstream f("input.txt");
int times_2 = 0,times_3 = 0;

 for (auto it = std::istream_iterator<std::string>(f); it != std::istream_iterator<std::string>();++it)
 {
     std::map<char,int> my_map;
     for(auto c : *it)
     my_map[c]++;
    bool found2 = false,found3 = false;
    for(auto m: my_map)
    {
        if(!found2 && m.second == 2 )
        {
            times_2++;
            found2= true; 
        }
        if (!found3 && m.second == 3)
        {
            times_3++;
            found3 = true;
        }
     }   
 }
std::cout << times_2*times_3;
    return 0;
}

//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //

/*
counting the number that have an ID containing exactly two of any letter and then separately counting those with exactly three of any letter. You can multiply those two counts together to get a rudimentary checksum and compare it to what your device predicts.

For example, if you see the following box IDs:

abcdef contains no letters that appear exactly two or three times.
bababc contains two a and three b, so it counts for both.
abbcde contains two b, but no letter appears exactly three times.
abcccd contains three c, but no letter appears exactly two times.
aabcdd contains two a and two d, but it only counts once.
abcdee contains two e.
ababab contains three a and three b, but it only counts once.
Of these box IDs, four of them contain a letter which appears exactly twice, and three of them contain a letter which appears exactly three times. Multiplying these together produces a checksum of 4 * 3 = 12.
*/