#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>

struct coordinate{
    int x;
    int y;
};
struct data
{
   std::string id;
   coordinate starting_coord;
   coordinate size;
};

int main()
{
    constexpr int size= 1000;
    int **fabric = new  int*[size];
    for(int i = 0;i < size;++i)
        fabric[i] = new int[size];

    for(int i= 0;i < size;++i)
        for(int j= 0;j < size;++j)
        fabric[i][j] = 0;

    std::ifstream f("input.txt");

    for(auto it = std::istream_iterator<std::string>(f);it != std::istream_iterator<std::string>();++it)
    {
        data dat;

        std::string str;//std::string str= *it;   //#id
        //str.erase(std::find(str.begin(),str.end(),'#'));
        //dat.id = str; 
        ++it;  //@
        ++it;  //146,196:

        std::string str2 = *it;

        auto comma = std::find(str2.begin(),str2.end(),',');
        str = std::string(str2.begin(),comma);

        dat.starting_coord.x = std::stoi(str);

        str = std::string(comma+1,str2.end()-1);
        dat.starting_coord.y = std::stoi(str);

        ++it;  //19x14
        str2 = *it;
        comma = std::find(str2.begin(),str2.end(),'x');
                str = std::string(str2.begin(),comma);
        dat.size.x = std::stoi(str);
        str = std::string(comma+1,str2.end());
        dat.size.y = std::stoi(str);

        for (int i = dat.starting_coord.x;i < dat.starting_coord.x+dat.size.x;++i)
            for(int j = dat.starting_coord.y;j < dat.starting_coord.y+dat.size.y;++j)
                fabric[i][j]++;
    }

    int sum = 0;

    for(int i= 0;i < size;++i)
        for(int j= 0;j < size;++j)
            if (fabric[i][j] > 1)
                sum++;
				
    std::cout << sum;

for(int i = 0;i < size;++i)
    delete[] fabric[i] ;
delete[] fabric;

return 0;
}

//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //

/*
The problem is that many of the claims overlap, causing two or more claims to cover part of the same areas. For example, consider the following claims:

#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2
Visually, these claim the following areas:

........
...2222.
...2222.
.11XX22.
.11XX22.
.111133.
.111133.
........
The four square inches marked with X are claimed by both 1 and 2. (Claim 3, while adjacent to the others, does not overlap either of them.)

If the Elves all proceed with their own plans, none of them will have enough fabric. How many square inches of fabric are within two or more claims?
*/
