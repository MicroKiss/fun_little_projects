#include <iostream>
#include <set>
#include <vector>
int main()
{
	
int sum = 0;
bool found= false;
std::string tmp;
std::set<int> my_set;
std::vector<int> my_vector;

while (!found)
{
	if(my_set.count(sum))
	{
		found = true;
		break;
	}
	my_set.insert(sum);
	std::cin >> tmp;
	if(tmp == "00")
		break;
	sum += std::stoi(tmp);
	my_vector.push_back(std::stoi(tmp));
}

auto it = my_vector.begin();

while (!found)
{
	sum+= *it;
	it == my_vector.end()-1 ? it = my_vector.begin(): ++it;
	if (my_set.count(sum))
		found = true;
}
std::cout << "the answer is: "<<sum << std::endl;
return 0;	
}



//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //



/*
--- Part Two ---
You notice that the device repeats the same frequency change list over and over. To calibrate the device, you need to find the first frequency it reaches twice.

For example, using the same list of changes above, the device would loop as follows:

Current frequency  0, change of +1; resulting frequency  1.
Current frequency  1, change of -2; resulting frequency -1.
Current frequency -1, change of +3; resulting frequency  2.
Current frequency  2, change of +1; resulting frequency  3.
(At this point, the device continues from the start of the list.)
Current frequency  3, change of +1; resulting frequency  4.
Current frequency  4, change of -2; resulting frequency  2, which has already been seen.
In this example, the first frequency reached twice is 2. Note that your device might need to repeat its list of frequency changes many times before a duplicate frequency is found, and that duplicates might be found while in the middle of processing the list.

Here are other examples:

+1, -1 first reaches 0 twice.
+3, +3, +4, -2, -4 first reaches 10 twice.
-6, +3, +8, +5, -6 first reaches 5 twice.
+7, +7, -2, -7, -4 first reaches 14 twice.
What is the first frequency your device reaches twice?

*/