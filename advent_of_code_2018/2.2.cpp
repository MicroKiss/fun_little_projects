#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <string>

int main()
{
std::ifstream f("input.txt");

std::vector<std::string> my_vec;

 for (auto it = std::istream_iterator<std::string>(f); it != std::istream_iterator<std::string>();++it)
    my_vec.push_back(*it);

auto almost_equal = [](std::string a,std::string b, int& pos)  // for same sized strings 
{
int diff = 0;
for(int i = 0;i < a.length();++i)
{
    if(a[i] != b[i])
    {
        diff++;
         pos =i;
    }
    if (diff > 1)
        return false;
}
    return true;
};

for (auto it1 = my_vec.begin(); it1 != my_vec.end()-1;++ it1) {
   for (auto it2 = it1+1;it2 != my_vec.end();++it2) {

       int pos;
       if(almost_equal(*it1,*it2,pos))
        {
           std::cout << std::string((*it1)).erase(pos,1);
           return 0;
        }
    }     
}
    return 0;
}



//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //


/*

The boxes will have IDs which differ by exactly one character at the same position in both strings. For example, given the following box IDs:

abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz
The IDs abcde and axcye are close, but they differ by two characters (the second and fourth). However, the IDs fghij and fguij differ by exactly one character, the third (h and u). Those must be the correct boxes.

What letters are common between the two correct box IDs? (In the example above, this is found by removing the differing character from either ID, producing fgij.)

*/







