#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>

struct coordinate{
    int x;
    int y;
};
struct data
{
   std::string id;
   coordinate starting_coord;
   coordinate size;
};

int main()
{
    constexpr int size= 1000;
    int **fabric = new  int*[size];
    for(int i = 0;i < size;++i)
        fabric[i] = new int[size];

    for(int i= 0;i < size;++i)
        for(int j= 0;j < size;++j)
        fabric[i][j] = 0;

    std::ifstream f("input.txt");

    for(auto it = std::istream_iterator<std::string>(f);it != std::istream_iterator<std::string>();++it)
    {
        data dat;

        std::string str;//std::string str= *it;   //#id
        //str.erase(std::find(str.begin(),str.end(),'#'));
        //dat.id = str; 
        ++it;  //@
        ++it;  //146,196:

        std::string str2 = *it;

        auto comma = std::find(str2.begin(),str2.end(),',');
        str = std::string(str2.begin(),comma);

        dat.starting_coord.x = std::stoi(str);

        str = std::string(comma+1,str2.end()-1);
        dat.starting_coord.y = std::stoi(str);

        ++it;  //19x14
        str2 = *it;
        comma = std::find(str2.begin(),str2.end(),'x');
                str = std::string(str2.begin(),comma);
        dat.size.x = std::stoi(str);
        str = std::string(comma+1,str2.end());
        dat.size.y = std::stoi(str);

        for (int i = dat.starting_coord.x;i < dat.starting_coord.x+dat.size.x;++i)
            for(int j = dat.starting_coord.y;j < dat.starting_coord.y+dat.size.y;++j)
                fabric[i][j]++;
    }

    std::ifstream f2("input.txt");

    for(auto it = std::istream_iterator<std::string>(f2);it != std::istream_iterator<std::string>();++it)
    {
        data dat;

        std::string str= *it;   //#id
        str.erase(std::find(str.begin(),str.end(),'#'));
        dat.id = str; 
        ++it;  //@
        ++it;  //146,196:

        std::string str2 = *it;

        auto comma = std::find(str2.begin(),str2.end(),',');
        str = std::string(str2.begin(),comma);

        dat.starting_coord.x = std::stoi(str);

        str = std::string(comma+1,str2.end()-1);
        dat.starting_coord.y = std::stoi(str);

        ++it;  //19x14
        str2 = *it;
        comma = std::find(str2.begin(),str2.end(),'x');
                str = std::string(str2.begin(),comma);
        dat.size.x = std::stoi(str);
        str = std::string(comma+1,str2.end());
        dat.size.y = std::stoi(str);

        bool l = true;

        for (int i = dat.starting_coord.x;i < dat.starting_coord.x+dat.size.x;++i)
            for(int j = dat.starting_coord.y;j < dat.starting_coord.y+dat.size.y;++j)
                l = (fabric[i][j] == 1) && l;

        if(l)
        {
            std::cout << dat.id;
            break;
        }
    }

for(int i = 0;i < size;++i)
    delete[] fabric[i] ;
delete[] fabric;

return 0;
}

//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //

/*
Amidst the chaos, you notice that exactly one claim doesn't overlap by even a single square inch of fabric with any other claim. If you can somehow draw attention to it, maybe the Elves will be able to make Santa's suit after all!

For example, in the claims above, only claim 3 is intact after all claims are made.

What is the ID of the only claim that doesn't overlap?
*/
