//Conway's Game of Life -for windows
//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <chrono>
#include <thread>
#include <windows.h>


using namespace std;
int main()
{

    srand(time(NULL));
 HANDLE  hConsole = GetStdHandle(STD_OUTPUT_HANDLE);


auto write=[hConsole](int**t,int size)
{
	for (int i = 0;i < size ;++i )
	{
		for (int j = 0;j < size ;++j)
		{
			if(t[i][j])
				SetConsoleTextAttribute(hConsole, 10); //green text
			else
				SetConsoleTextAttribute(hConsole, 8);// gray text

			std::cout << t[i][j]<< " ";
		}
		endl(cout);
	}
};



int size = 16;
int **table = new int*[size];
int **new_table = new int*[size];

for (int i = 0;i < size ;++i )
    table[i] = new int[size];
for (int i = 0;i < size ;++i )
    new_table[i] = new int[size];


for (int i = 0;i < size ; ++i)
{
    for (int j = 0;j < size ;++j )
    {
        table[i][j] = rand() % 2;
    }
}

    write(table,size);


while(1){
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
       system("cls");


    for(int i = 1;i < size-1;++i)
    {
        for(int j = 1;j < size-1;++j)
        {
            int neighbours = table[i-1][j-1] + table[i-1][j] + table[i-1][j+1]+
                             table[i][j-1]   +       0       + table[i][j+1]+
                             table[i+1][j-1] + table[i+1][j] + table[i+1][j+1];

            if(table[i][j])
            {
                if(neighbours < 2 || neighbours> 3 )
                    new_table[i][j] = 0;
                else
                    new_table[i][j] = 1;
            }
            else
            {//dead
                if(neighbours == 3)
                    new_table[i][j] = 1;
            }
        }
    }

    for (int i = 0;i < size ;++i )
    {
        for (int j = 0;j <size ;++j )
        {
            table[i][j] = new_table[i][j];
        }
    }

    write(table,size);
}///while TRUE

return 0x2A;
}
